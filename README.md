# README #

Repositorio de arquivos de prontuários de pacientes com tumor cerebral do CPC-HIAS.

Protocolos de tratamento usados no Centro Pediátrico do Câncer, baseados em ensaios clínicos de grupos cooperativos. Esboços e versões antigas (depreciadas), bem como versões alfa e beta.

### Função desse repositório ###

* Armazenamento em nuvem
* Versionamento do atendimento aos pacientes
* Testar tecnologias de automação para os documentos

### Autoria e responsabilidade ###

* Francisco H C Felix, Pediatra Cancerologista
* Centro Pediátrico do Câncer - Hospital Infantil Albert Sabin
